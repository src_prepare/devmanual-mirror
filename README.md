# Devmanual Mirror

<p align="center">
    <a href="https://gitlab.com/src_prepare/devmanual-mirror/pipelines">
        <img src="https://gitlab.com/src_prepare/devmanual-mirror/badges/master/pipeline.svg">
    </a>
    <a href="https://gitlab.com/src_prepare/devmanual-mirror/">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/hosted_on-gitlab-orange.svg">
    </a>
    <a href="https://gentoo.org/">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/powered-by-gentoo-linux-tyrian.svg">
    </a>
    <a href="./LICENSE">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/license-gplv3-blue.svg">
    </a>
    <a href="https://app.element.io/#/room/#src_prepare:matrix.org">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/chat-matrix-green.svg">
    </a>
    <a href="https://gitlab.com/src_prepare/devmanual-mirror/commits/master.atom">
        <img src="https://gitlab.com/src_prepare/badge/-/raw/master/feed-atom-orange.svg">
    </a>
</p>


# About

Mirror Gentoo's devmanual with GitLab pages.


# License


## Gentoo's devmanual

The devmanual is licensed under "Attribution-ShareAlike 4.0 International" license.

More info: https://gitweb.gentoo.org/proj/devmanual.git/tree/LICENSE


## src_prepare's devmanual-mirror

Scripts in this repository are licensed under GPL-3

SPDX-License-Identifier: GPL-3.0-only

### Unless otherwise stated contents here are under the GNU GPL v3 license

This file is part of devmanual-mirror.

devmanual-mirror is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

devmanual-mirror is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with devmanual-mirror.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2020-2021, src_prepare group
Licensed under the GNU GPL v3 License
