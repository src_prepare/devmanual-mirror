#!/bin/sh


# This file is part of devmanual-mirror.

# devmanual-mirror is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# devmanual-mirror is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with devmanual-mirror.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć <xgqt@riseup.net>
# Copyright (c) 2020, src_prepare group
# Licensed under the GNU GPL v3 License


trap 'exit 128' INT
set -e
export PATH


PN="devmanual"
PV="0_pre20200207"
P="${PN}-${PV}"
SRC="${P}.tar.xz"
SRC_URI="https://dev.gentoo.org/~ulm/distfiles/${SRC}"

echo "Running with:
 - PN:       ${PN}
 - PV:       ${PV}
 - P:        ${P}
 - SRC:      ${SRC}
 - SRC_URI:  ${SRC_URI}
"


sh ./clean.sh

wget "${SRC_URI}"
tar xJpf "${SRC}"

cd "${PN}"

cd bin
patch -li ../../files/url_root.patch
cd ..

make OFFLINE=0
make OFFLINE=0 DESTDIR="" htmldir="public" install

mv ./public ../public
